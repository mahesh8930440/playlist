import React from "react";

const INITIAL_VIDEO_ITEMS = [
  {
    id: 1,
    title: "video1",
    url: "https://youtube.com/v/e9r9349394",
    isMarkedAsCompleted: false,
    length: 300,
  },
  {
    id: 2,
    title: "video2",
    url: "https://youtube.com/v/e9r9349394",
    isMarkedAsCompleted: false,
    length: 300,
  },
  {
    id: 3,
    title: "video3",
    url: "https://youtube.com/v/e9r9349394",
    isMarkedAsCompleted: false,
    length: 300,
  },
  {
    id: 4,
    title: "video4",
    url: "https://youtube.com/v/e9r9349394",
    isMarkedAsCompleted: false,
    length: 300,
  },
  {
    id: 5,
    title: "video5",
    url: "https://youtube.com/v/e9r9349394",
    isMarkedAsCompleted: true,
    length: 300,
  },
  {
    id: 6,
    title: "video6",
    url: "https://youtube.com/v/e9r9349394",
    isMarkedAsCompleted: true,
    length: 300,
  },
];

export default function Playlist() {
  const [videoItems, setVideoItems] = React.useState(INITIAL_VIDEO_ITEMS);

  const [currentVideoId, setCurrentVideoId] = React.useState(3);

  function onVideoSelected(videoId) {
    setCurrentVideoId(videoId);
  }

  const currentVideo = videoItems.find(
    (videoItem) => videoItem.id === currentVideoId
  );
  
  function changeStatusOfVideo(video) {
    setVideoItems((prevVideoItems) =>
      prevVideoItems.map((item) =>
        item.id === video.id
          ? { ...item, isMarkedAsCompleted: !item.isMarkedAsCompleted }
          : item
      )
    );
  }

  return (
    <div className="playlist">
      <header>
        <h1 className="playlist-heading">Playlist</h1>
      </header>
      <div className="content-container">
        <main>
          <CurrentVideoTitle video={currentVideo} />
          <VideoPlayer video={currentVideo} changeStatusOfVideo={changeStatusOfVideo}/>
        </main>
        <Sidebar
          videoItems={videoItems}
          currentVideoId={currentVideoId}
          setCurrentVideoId={onVideoSelected}
        />
      </div>
    </div>
  );
}

function CurrentVideoTitle({ video }) {
  return <div className="current-video-title">{video.title}</div>;
}


function VideoPlayer({ video ,changeStatusOfVideo}) {
  return (
    <div className="video-player-container">
      <div>Video Player</div>
      <div>
        <pre>
          <code>{JSON.stringify(video, null, 4)}</code>
        </pre>
      </div>
      <button onClick={()=>changeStatusOfVideo(video)}>{video.isMarkedAsCompleted? "Mark Video As completed":"Unmark video As completed"}</button>
    </div>
  );
}

function Sidebar({ videoItems, currentVideoId, setCurrentVideoId }) {
  return (
    <div className="sidebar">
      <WatchedVideosProgress videoItems={videoItems}/>
      <VideoItemList
        videoItems={videoItems}
        currentVideoId={currentVideoId}
        setCurrentVideoId={setCurrentVideoId}
      />
    </div>
  );
}

function WatchedVideosProgress({videoItems}) {
  const countOfWatchedVideos=videoItems.filter((video)=> video.isMarkedAsCompleted).length;
  console.log(countOfWatchedVideos);
  return <div className="watched-videos-progress">{countOfWatchedVideos}-WatchedVideosProgress</div>;
}

function VideoItemList({ videoItems, currentVideoId, setCurrentVideoId }) {
  return (
    <div className="video-item-list">
      <ul>
        {videoItems.map((item) => {
          let activeClassName = item.id === currentVideoId ? "active-item" : "";
          function handleClick() {
            setCurrentVideoId(item.id);
          }
          return (
            <li
              key={item.id}
              className={`${activeClassName}`}
              onClick={handleClick}
            >
            {item.isMarkedAsCompleted?(<span><i class="fa-regular fa-square-check"></i>{item.title}</span>):<span>{item.title}</span>}
            </li>
          );
        })}
      </ul>
    </div>
  );
}